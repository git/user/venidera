# Venidera Portage Overlay

This is Venidera's Portage Overlay for the Gentoo Linux operating system.
The purpose of this set of software configuration scripts is to enable
developers and system administrators to easily comply with Venidera's
policies for software development and deployment.

_Copyright &copy; 2012, Venidera Pesquisa & Desenvolvimento, Ltda. Licensed under
the terms of the GNU General Public License version 2. Please see the LICENSE
file. All rights reserved._
