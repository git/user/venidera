# Roadmap

This roadmap lists future inclusions and developments.

## dev-db/neo4j-community

* Make PID and lock file handling Gentoo-style
* Add neo4j user to the system
* Clean up service clutter and Windows/Mac OSX references from bin/neo4j and bin/utils
* Add USE flag for optional documentation installation

_Copyright &copy; 2012, Venidera Pesquisa & Desenvolvimento, Ltda. Licensed under
the terms of the GNU General Public License version 2. Please see the LICENSE
file. All rights reserved._
